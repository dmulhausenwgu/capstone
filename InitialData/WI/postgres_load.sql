ALTER TABLE nibrs_arrestee DISABLE TRIGGER ALL;
ALTER TABLE nibrs_arrestee_weapon DISABLE TRIGGER ALL;
ALTER TABLE nibrs_bias_motivation DISABLE TRIGGER ALL;
ALTER TABLE nibrs_month DISABLE TRIGGER ALL;
ALTER TABLE nibrs_incident DISABLE TRIGGER ALL;
ALTER TABLE nibrs_offender DISABLE TRIGGER ALL;
ALTER TABLE nibrs_offense DISABLE TRIGGER ALL;
ALTER TABLE nibrs_property DISABLE TRIGGER ALL;
ALTER TABLE nibrs_property_desc DISABLE TRIGGER ALL;
ALTER TABLE nibrs_suspect_using DISABLE TRIGGER ALL;
ALTER TABLE nibrs_suspected_drug DISABLE TRIGGER ALL;
ALTER TABLE nibrs_victim DISABLE TRIGGER ALL;
ALTER TABLE nibrs_victim_circumstances DISABLE TRIGGER ALL;
ALTER TABLE nibrs_victim_injury DISABLE TRIGGER ALL;
ALTER TABLE nibrs_victim_offender_rel DISABLE TRIGGER ALL;
ALTER TABLE nibrs_victim_offense DISABLE TRIGGER ALL;
ALTER TABLE nibrs_weapon DISABLE TRIGGER ALL;
ALTER TABLE nibrs_criminal_act DISABLE TRIGGER ALL;

COPY agencies FROM 'C:\projects\WGU\Capstone\data\WI\agencies.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_ACTIVITY_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_ACTIVITY_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_AGE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_AGE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_ARREST_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_ARREST_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_ASSIGNMENT_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_ASSIGNMENT_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_BIAS_LIST FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_BIAS_LIST.csv' DELIMITER ',' HEADER CSV encoding 'windows-1251';
COPY NIBRS_LOCATION_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_LOCATION_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_OFFENSE_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_OFFENSE_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_PROP_LOSS_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_PROP_LOSS_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_PROP_DESC_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_PROP_DESC_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_CIRCUMSTANCES FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_CIRCUMSTANCES.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_CLEARED_EXCEPT FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_CLEARED_EXCEPT.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_CRIMINAL_ACT FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_CRIMINAL_ACT.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_CRIMINAL_ACT_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_CRIMINAL_ACT_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_DRUG_MEASURE_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_DRUG_MEASURE_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_ETHNICITY FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_ETHNICITY.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_INJURY FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_INJURY.csv' DELIMITER ',' HEADER CSV;


COPY NIBRS_RELATIONSHIP FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_RELATIONSHIP.csv' DELIMITER ',' HEADER CSV;


COPY NIBRS_SUSPECTED_DRUG_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_SUSPECTED_DRUG_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_USING_LIST FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_USING_LIST.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_WEAPON_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_WEAPON_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY REF_RACE FROM 'C:\projects\WGU\Capstone\data\WI\REF_RACE.csv' DELIMITER ',' HEADER CSV;
COPY REF_STATE FROM 'C:\projects\WGU\Capstone\data\WI\REF_STATE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_ARRESTEE_WEAPON FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_ARRESTEE_WEAPON.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_BIAS_MOTIVATION FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_BIAS_MOTIVATION.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_VICTIM_TYPE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_VICTIM_TYPE.csv' DELIMITER ',' HEADER CSV;
COPY NIBRS_JUSTIFIABLE_FORCE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_JUSTIFIABLE_FORCE.csv' DELIMITER ',' HEADER CSV;


COPY NIBRS_ARRESTEE FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_ARRESTEE.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_month FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_month.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_incident FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_incident.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_offender FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_OFFENDER.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_offense FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_OFFENSE.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_property FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_PROPERTY.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_property_desc FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_PROPERTY_DESC.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_suspect_using FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_SUSPECT_USING.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_suspected_drug FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_SUSPECTED_DRUG.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_victim FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_VICTIM.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_victim_circumstances FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_VICTIM_CIRCUMSTANCES.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_victim_injury FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_VICTIM_INJURY.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_victim_offender_rel FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_VICTIM_OFFENDER_REL.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_victim_offense FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_VICTIM_OFFENSE.csv' DELIMITER ',' HEADER CSV;
COPY nibrs_weapon FROM 'C:\projects\WGU\Capstone\data\WI\NIBRS_WEAPON.csv' DELIMITER ',' HEADER CSV;


ALTER TABLE nibrs_arrestee ENABLE TRIGGER ALL;
ALTER TABLE nibrs_arrestee_weapon ENABLE TRIGGER ALL;
ALTER TABLE nibrs_bias_motivation ENABLE TRIGGER ALL;
ALTER TABLE nibrs_month ENABLE TRIGGER ALL;
ALTER TABLE nibrs_incident ENABLE TRIGGER ALL;
ALTER TABLE nibrs_offender ENABLE TRIGGER ALL;
ALTER TABLE nibrs_offense ENABLE TRIGGER ALL;
ALTER TABLE nibrs_property ENABLE TRIGGER ALL;
ALTER TABLE nibrs_property_desc ENABLE TRIGGER ALL;
ALTER TABLE nibrs_suspect_using ENABLE TRIGGER ALL;
ALTER TABLE nibrs_suspected_drug ENABLE TRIGGER ALL;
ALTER TABLE nibrs_victim ENABLE TRIGGER ALL;
ALTER TABLE nibrs_victim_circumstances ENABLE TRIGGER ALL;
ALTER TABLE nibrs_victim_injury ENABLE TRIGGER ALL;
ALTER TABLE nibrs_victim_offender_rel ENABLE TRIGGER ALL;
ALTER TABLE nibrs_victim_offense ENABLE TRIGGER ALL;
ALTER TABLE nibrs_weapon ENABLE TRIGGER ALL;
ALTER TABLE nibrs_criminal_act ENABLE TRIGGER ALL;