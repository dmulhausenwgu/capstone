library(psych)
library(caret)
setwd("C:/Projects/WGU/capstone/ProjectData")
fn<-'data1.csv'
data<-read.csv(file=fn)
df<-data.frame(data)
describe(df)
fn<-'postPCA.csv'
data<-read.csv(file=fn)
df<-data.frame(data)
fn<-'https://drive.google.com/file/d/1ZuNkA_K9WTOCfudyr_Ge1fnKdpYfrQlA/view?usp=sharing'
data<-read.csv(file=fn)
id<-'1ZuNkA_K9WTOCfudyr_Ge1fnKdpYfrQlA'
x<-read.csv(sprintf("https://docs.google.com/uc?id=%s&export=download", id))  
xdf<-data.frame(x)
head(x)
str(df)
head(df)
lm1<-train(SatisfiedCustomer~.^2, data=df, method='lmStepAIC', direction='both')
lm1$fullModel
library(mclust)
fit<-Mclust(xdf)
plot(fit)
